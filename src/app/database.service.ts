import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {

  constructor(private ht: HttpClient) { }

  storeEmp(user) {
    return this.ht.post (
      'https://firstangular-hmonat.firebaseio.com/users.json', user
    );

  }
   getEmp() {
    return this.ht.get('https://firstangular-hmonat.firebaseio.com/users.json');
   }
}
