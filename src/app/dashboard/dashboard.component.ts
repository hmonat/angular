import { Component, OnInit } from '@angular/core';
import { AccountService } from '../account.service';
import { Router, ActivatedRoute } from '../../../node_modules/@angular/Router';
import { EmailServiceService } from '../email-service.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  constructor(
    private ms: AccountService,
    private route: ActivatedRoute,
    private routes: Router,
    private ess: EmailServiceService
  ) {
    this.ess.emailPass.subscribe((email: string) => { console.log(this.email = email); });

  }

  email;
  x = true;

  ngOnInit() {

    document.getElementsByTagName('nav')[0].style.display = 'none';
    const obj = this.ms.getInfo(this.email);
    console.log(this.email);

       this.createTable(obj);

  }
  logout() {

    if (this.ms.logout(this.x)) {
      this.x = true;
    } else {
      this.x = false;
    }

 }

  createTable(i) {
    console.log(i);
    const tr = document.createElement('tr');

    const td1 = document.createElement('td');
    td1.innerHTML = i.name;
    const td2 = document.createElement('td');
    td2.innerHTML = i.email;
    const td3 = document.createElement('td');
    td3.innerHTML = i.age;
    const td4 = document.createElement('td');
    td4.innerHTML = i.contact;
    const td5 = document.createElement('td');
    td5.innerHTML = i.password;

    tr.appendChild(td1);
    tr.appendChild(td2);
    tr.appendChild(td3);
    tr.appendChild(td4);
    tr.appendChild(td5);

    document.getElementById('tabBody').appendChild(tr);
  }
}
