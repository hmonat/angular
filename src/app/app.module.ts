import { DatabaseService } from './database.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { RouterModule, Routes } from '@angular/Router';
import {APP_BASE_HREF} from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AccountService } from './account.service';
import { ReactiveFormsModule, FormGroup, FormControl } from '@angular/forms';
import { EmailServiceService } from './email-service.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';



const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  {
    path: 'dashboard',
    component: DashboardComponent,
  }
];

@NgModule({
  declarations: [AppComponent, LoginComponent, SignupComponent, DashboardComponent],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [AccountService, EmailServiceService, DatabaseService, HttpClient , {provide: APP_BASE_HREF, useValue : '/' }],
  bootstrap: [AppComponent]
})
export class AppModule {}
