import { DatabaseService } from './../database.service';
import { Component, OnInit } from '@angular/core';
import { AccountService } from '../account.service';
import { Router, ActivatedRoute } from '../../../node_modules/@angular/Router';
import {
  ReactiveFormsModule,
  FormGroup,
  FormControl,
  Validators,
  ValidatorFn,
  AbstractControl
} from '@angular/forms';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  signUpForm;

  constructor(
    private accountService: AccountService,
    private router: Router,
    private route: ActivatedRoute,
    private ds: DatabaseService
  ) {}

  ngOnInit() {
    this.signUpForm = new FormGroup({
      name: new FormControl('Surya', [
        Validators.required,
        this.forbiddenNameValidator(/Sagar/i)
      ]),
      email: new FormControl('himanshu.monat@xoriant.com', [
        Validators.required,
        Validators.email
      ]),
      age: new FormControl('22', [
        Validators.required,
        Validators.max(60),
        Validators.min(18)
      ]),
      contact: new FormControl('7747983422', [
        Validators.required,
        Validators.max(9999999999),
        Validators.min(6000000000)
      ]),
      pwd: new FormControl('1111', Validators.required)
    });


  }

  register() {
    console.log(this.signUpForm.value);
    const prop = this.signUpForm.value;
    const obj = this.accountService.register(
      prop.name,
      prop.email,
      prop.age,
      prop.contact,
      prop.pwd
    );

    this.ds.storeEmp(obj)
    .subscribe(
      (response) => {},
      (error) => {}
    )
    ;
    this.router.navigate(['/login'], { relativeTo: this.route });
  }

  forbiddenNameValidator(nameRe: RegExp): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const forbidden = nameRe.test(control.value);
      return forbidden ? { forbiddenName: { value: control.value } } : null;
    };
  }
}
