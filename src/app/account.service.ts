import { DatabaseService } from './database.service';
import { Router } from '../../node_modules/@angular/Router';
import { Injectable } from '../../node_modules/@angular/core';
import { EmailServiceService } from './email-service.service';

@Injectable()
export class AccountService {

  allEmps;

  employees: {
    name: string;
    email: string;
    age: number;
    contact: number;
    password: string;
  }[] = [];

  constructor(private route: Router, private ess: EmailServiceService, private ds: DatabaseService ) { }

  register(name, email, age, contact, password) {
    const obj = {
      name: name,
      email: email,
      age: age,
      contact: contact,
      password: password
    };
    this.employees.push(obj);
    return obj;
  }

  login(email, pwd) {
  for (const e of this.employees) {

      if (email === e.email && pwd === e.password) {
        this.ess.emailPass.next(email);
        this.route.navigate(['/dashboard']);
        return true;
      }
    }
    return false;
  }

  getInfo(email): {}[] | {} {
    if (email === null) {
      return this.employees;
    } else {
      for (const e of this.employees) {
        if (email === e.email) {
          return e;
        }
      }
    }
  }
  logout(ses) {
      if (!ses) {
        this.route.navigate(['/dashboard']);
        return true;
      } else {
        document.getElementsByTagName('nav')[0].style.display = 'flex';
        this.route.navigate(['/login']);
        return false;
      }
  }

  getAllEmp(email, password) {

    this.ds.getEmp().subscribe(
      response => {

        this.allEmps = response;

          for (let key in this.allEmps) {
            if (this.allEmps.hasOwnProperty(key)) {
                const val = this.allEmps[key];
                // console.log(
                //   val['name'] + '---' + val['email'] + '---' + val['age'] + '---' + val['contact'] + '---' + val['password']
                // );
                console.log( val['email'] + '===' + email  + '===' +  val['password']  + '===' +  password);
                if ( val['email'] === email && val['password'] === password) {
                  alert('You are logged in');
                  this.route.navigate(['/dashboard']);
                  return true;
              }

                }
            }
          },

      error => {}

    );
  }
}
