import { Component, OnInit } from '@angular/core';
import { AccountService } from '../account.service';
import { EmailServiceService } from '../email-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  x = false;
  constructor(
    private as: AccountService
  ) {}

  ngOnInit() {
    // console.log('hfs');
   // this.as.getAllEmp();
  }

  login (email, pwd) {
    if (this.as.getAllEmp(email, pwd)) {
    } else {
      alert('Email-Pwd doesn\'t match');
    }
  }
  logout() {
     if (this.as.logout(this.x)) {
      this.x = true;
    } else {
      this.x = false;
    }
  }

}
